﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using lab2.MapReduce.Logic;

namespace lab2.MapReduce.MapperWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            var channelFactory = new DuplexChannelFactory<IMasterMapper>(new Mapper(), "*");
            var channel = channelFactory.CreateChannel();
            channel.RegisterMapper();
            Console.WriteLine("Mapper registered");
            Console.ReadLine();
            try
            {
                channel.UnsubscribeMe();
                Console.WriteLine("Unsubscribed");
            }
            catch (Exception e)
            {
                Console.WriteLine("Server is down");
            }

        }
    }
}
