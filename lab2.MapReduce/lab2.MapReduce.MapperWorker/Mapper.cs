﻿using lab2.MapReduce.Logic;

namespace lab2.MapReduce.MapperWorker
{
    public class Mapper:IMapper
    {
        public object Map(IMapOperation operation, object input)
        {
            return operation.Map(input);
        }
    }
}