﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace lab2.MapReduce.MasterNode
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(MapReduceService)))
            {
                host.Open();
                Console.WriteLine("Master node started. (Press enter to stop)");
                Console.ReadLine();
                host.Close();
            }
        }
    }
}
