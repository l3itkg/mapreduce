﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using lab2.MapReduce.Logic;

namespace lab2.MapReduce.MasterNode
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MapReduceService" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class MapReduceService : IMapReduceService, IMasterMapper, IMasterReducer
    {
        private readonly SynchronizedCollection<IReducer> _reducers = new SynchronizedCollection<IReducer>();
        private readonly SynchronizedCollection<IMapper> _mappers = new SynchronizedCollection<IMapper>();

        //Todo make method to register a new reducer (also make a new console project for reducer/mapper)
        public void RegisterReducer()
        {
            _reducers.Add(OperationContext.Current.GetCallbackChannel<IReducer>() ??
                         throw new Exception($"Service doesn't implement {nameof(IReducer)}."));
            Console.WriteLine($"New {nameof(IReducer)} was added (Total {_reducers.Count})");
        }
        //Todo make method to register a new mapper
        public void RegisterMapper()
        {
            _mappers.Add(OperationContext.Current.GetCallbackChannel<IMapper>() ??
                        throw new Exception($"Service doesn't implement {nameof(IMapper)}."));
            Console.WriteLine($"New {nameof(IMapper)} was added (Total {_mappers.Count})");
        }

        //Todo unsubscribe
        public void UnsubscribeMe()
        {
            try
            {
                var reducer = OperationContext.Current.GetCallbackChannel<IReducer>();
                if (reducer != null) _reducers.Remove(reducer);
                return;
            }
            catch (InvalidCastException)
            {
            }

            try
            {
                var mapper = OperationContext.Current.GetCallbackChannel<IMapper>();
                if (mapper != null) _mappers.Remove(mapper);
                return;
            }
            catch (InvalidCastException e)
            {
            }
            Console.WriteLine("Not a IReducer or IMapper asks to unsubscribe");
        }

        public object[] DoWork(object[] inputTasks, IMapOperation mapOperation, IReduceOperation[] reduceOperations)
        {
            if (_reducers.Count<1) throw new Exception("No reduceres to complete task.");
            if (_mappers.Count < 1) throw new Exception("No Mappers to complete task.");

            var reduceOperationsQueue = new ConcurrentQueue<(int index,IReduceOperation operation)>(reduceOperations.Select((r,i) => (i,r)));

            ConcurrentQueue<object> tasks = new ConcurrentQueue<object>(inputTasks);
            var mappedResults = new List<ConcurrentQueue<object>>();
            for (int i = 0; i < reduceOperations.Length; i++)
            {
                mappedResults.Add(new ConcurrentQueue<object>());
            }
            var mapCompleted = new CancellationTokenSource();
            var token = mapCompleted.Token;

            var mapWorking = Task.Factory.StartNew(() =>
                Parallel.ForEach(_mappers, (m) =>
                {
                    while (true)
                    {
                        if (!tasks.TryDequeue(out var task))
                        {
                            return;
                        }

                        var mapped = m.Map(mapOperation, task);
                        foreach (var reducerQueue in mappedResults)
                        {
                            reducerQueue.Enqueue(mapped);
                        }
                    }
                })
            );


            var reduceResults = new object[reduceOperations.Length];
            var reduceWorking = Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(_reducers, (r) =>
                {
                    while (reduceOperationsQueue.TryDequeue(out var reduceOperation))
                    {
                        var index = reduceOperation.index;
                        while (true)
                        {
                            if (!mappedResults[index].TryDequeue(out var task))
                            {
                                if (token.IsCancellationRequested) break;
                                Thread.Sleep(0);
                                continue;
                            }

                            r.Reduce(reduceOperation.operation, task);
                        }
                        reduceResults[index] = r.GetResult(reduceOperation.operation);
                    }
                });
            });

            mapWorking.Wait();
            mapCompleted.Cancel();
            reduceWorking.Wait();
            return reduceResults;
        }

    }
}
