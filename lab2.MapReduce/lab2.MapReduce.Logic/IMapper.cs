﻿using System.ServiceModel;
using Common;

namespace lab2.MapReduce.Logic
{
    [ServiceContract]
    public interface IMapper
    {
        [OperationContract]
        [UseNetDataContractSerializer]
        object Map(IMapOperation operation, object input);
    }
}