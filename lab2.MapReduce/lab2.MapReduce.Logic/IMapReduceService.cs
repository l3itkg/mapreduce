﻿using System.Collections.Generic;
using System.ServiceModel;
using Common;

namespace lab2.MapReduce.Logic
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMapReduceService" in both code and config file together.
    [ServiceContract]
    public interface IMapReduceService
    {
        [OperationContract]
        [UseNetDataContractSerializer]
        object[] DoWork(object[] inputTasks, IMapOperation mapper, IReduceOperation[] reducer);
    }
}