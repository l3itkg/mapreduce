﻿using System.ServiceModel;

namespace lab2.MapReduce.Logic
{
    [ServiceContract(CallbackContract = typeof(IMapper))]
    public interface IMasterMapper
    {
        [OperationContract]
        void RegisterMapper();
        [OperationContract]
        void UnsubscribeMe();
    }
}