﻿namespace lab2.MapReduce.Logic
{
    public interface IReduceOperation
    {
        object Reduce(object mapInput, object prevResult);

        object GetResult(object currentResultState);
    }
}