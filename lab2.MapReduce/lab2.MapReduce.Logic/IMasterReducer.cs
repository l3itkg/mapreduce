﻿using System.ServiceModel;

namespace lab2.MapReduce.Logic
{
    [ServiceContract(CallbackContract = typeof(IReducer))]
    public interface IMasterReducer
    {
        [OperationContract]
        void RegisterReducer();
        [OperationContract]
        void UnsubscribeMe();
    }
}