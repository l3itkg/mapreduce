﻿using System.ServiceModel;
using Common;

namespace lab2.MapReduce.Logic
{
    [ServiceContract]
    public interface IReducer
    {
        [OperationContract]
        [UseNetDataContractSerializer]
        void Reduce(IReduceOperation operation, object mapResult);

        [OperationContract]
        [UseNetDataContractSerializer]
        object GetResult(IReduceOperation operation);
    }
}