﻿using System.Runtime.Serialization;

namespace lab2.MapReduce.Logic
{
    
    public interface IMapOperation
    {
        object Map(object str);
    }
}