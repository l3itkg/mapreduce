﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using lab2.MapReduce.Logic;

namespace lab2.MapReduce.ReducerWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            var channelFactory = new DuplexChannelFactory<IMasterReducer>(new Reducer(), "*");
            var channel = channelFactory.CreateChannel();
            channel.RegisterReducer();
            Console.WriteLine("Reducer registered");
            Console.ReadLine();
            try
            {
                channel.UnsubscribeMe();
                Console.WriteLine("Unsubscribed");
            }
            catch (Exception e)
            {
                Console.WriteLine("Server is down");
            }
        }
    }
}
