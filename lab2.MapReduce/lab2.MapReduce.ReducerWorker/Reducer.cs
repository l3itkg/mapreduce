﻿using lab2.MapReduce.Logic;

namespace lab2.MapReduce.ReducerWorker
{
    public class Reducer:IReducer
    {
        private object prevResult;
        public void Reduce(IReduceOperation operation, object mapResult)
        {
            prevResult = operation.Reduce(mapResult, prevResult);
            return;
        }

        public object GetResult(IReduceOperation operation)
        {
            var result = operation.GetResult(prevResult);
            prevResult = null;
            return result;
        }
    }
}