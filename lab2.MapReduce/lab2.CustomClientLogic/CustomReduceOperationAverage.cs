﻿using System.Runtime.Serialization;
using lab2.MapReduce.Logic;

namespace lab2.CustomClientLogic
{
    [DataContract]
    public class CustomReduceOperationAverage:IReduceOperation
    {
        [DataContract]
        struct Statistics
        {
            public int LinesCount;
            public int WordsCount;
        }
        [UseNetDataContractSerializer]
        public object Reduce(object mapInput, object prevResult)
        {
            Statistics res;
            if (prevResult == null) res = new Statistics();
            else res = (Statistics) prevResult;
            res.LinesCount+=1;
            res.WordsCount += (int) mapInput;
            return res;
        }

        public object GetResult(object currentResult)
        {
            var res = (Statistics) currentResult;
            return res.WordsCount / res.LinesCount;
        }
    }
}