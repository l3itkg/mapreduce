﻿using System.Runtime.Serialization;
using lab2.MapReduce.Logic;

namespace lab2.CustomClientLogic
{
    //Todo export into new dll
    [DataContract]
    public class CustomReduceOperationSum:IReduceOperation
    {
        public object Reduce(object input, object prevResult)
        {
            var words = (int)(prevResult??0) + (int) input;
            return words;
        }

        public object GetResult(object currentResult)
        {
            return currentResult;
        }
    }
}