﻿using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using lab2.MapReduce.Logic;

namespace Custom
{
    //Todo export into new dll
    [DataContract]
    public class MapOperation : IMapOperation
    {
        public object Map(object input)
        {
            var str = input as string;
            return Regex.Matches(str, @"\w+").Count;
        }
    }
}