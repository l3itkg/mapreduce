﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using Custom;
using lab2.MapReduce.Logic;

namespace lab2.MapReduce.Client
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            //Wait until master start and mappers/reducers register
            Thread.Sleep(2000);
#endif

            var channelFactory = new ChannelFactory<IMapReduceService>("NetTcpBinding_IMapReduceService");
            var channel = channelFactory.CreateChannel();
            var tasks = new List<string>();
            using (var streamReader = new StreamReader(Environment.CurrentDirectory + "\\test.txt", Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    tasks.Add(line);
                }
            }


            var reduceOperations = new IReduceOperation[] {new ReduceOperation()};
            var mapOperation = new MapOperation();

            Console.WriteLine("Words Count: "+ channel.DoWork(tasks.ToArray(), mapOperation, reduceOperations).First());
            Console.ReadLine();
        }
    }
}
